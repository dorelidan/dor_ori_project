#include "Game.h"



Game::Game(const vector<User*>& players, int questionNo, DataBase& db) :_players(players), _db(db), _questions_no(questionNo)
{
	//1. use db copy con
	//2. uses DataBase::insertNewGame AND throwing excption if wasnt succesfull
	//3. if was succesfull: updating questions using DataBase::initQuestions
	//4. initilizes vars: players, results, and another thing
	Question* a = new Question(1, "How many baby chiks are killed every day in Israel?", "15,000", "2,000", "6,000", "1000");
	Question* b = new Question(1, "what happens to a baby cow?", "get caged in a small cage and slaughtered  in 3 weeks age", "get candies", "runs free in the farm", "staying with his mom");
	Question* c = new Question(1, "What is the average space for a chicken in the industries?", "A4 page", "100x100cm", "2meterX2meter", "1.5MeterX1.5Meter");
	Question* d = new Question(1, "Why cows have milk?", "They being raped to get pregnent by the dairy farmer while they are tied and can't resist", "they naturally have milk", "Because of god", "Jesus is responssible");
	for (auto player : _players) player->setGame(this);
	_questions.push_back(a);
	_questions.push_back(b);
	_questions.push_back(c);
	_questions.push_back(d);
	sendFirstQuestion();
}
Game::~Game()
{
	for (auto q : _questions) delete q;//not sure we can delete by using iterator auto
}


void Game::sendFirstQuestion()
{

	_currTurnAnswers = 0;
	_currQuestionIndex = 0;
	sendQuestionsToAllUsers();
}
void  Game::handleFinishGame(){}
bool Game::handleNextTurn() 
{
	if (_currQuestionIndex != _questions_no - 1)
	{
	_currTurnAnswers = 0;
	_currQuestionIndex++;
	sendQuestionsToAllUsers();
	}
	else
	{
		cout << "Finished";
	}
	return false; 
}
bool Game::handleAnswerFromUser(User* user, int que_n, int que_time){ 
	
	if (que_n == 5)
	{
		user->send("1200");
	}
	else if (que_n == _questions[_currQuestionIndex]->getCorrectAnswerIndex())
	{
		user->send("1201");
	}
	else
	{
		user->send("1200");
	}
	
	_currTurnAnswers++;

	if (_currTurnAnswers == _players.size())////check this
	{
		handleNextTurn();
	}

	
	return false; 

}



bool Game::leaveGame(User*){ return false; }
int Game::getID(){ return false; }
bool Game::insertGameToDB(){ return false; }
void Game::initQuestionsFromDB(){}
void Game::sendQuestionsToAllUsers()
{
	string msg = "118";
	try
	{
		msg += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getQuestion().size(), 3);
		msg += _questions[_currQuestionIndex]->getQuestion();
		msg += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getAnswers()[0].size(), 3);
		msg += _questions[_currQuestionIndex]->getAnswers()[0];
		msg += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getAnswers()[1].size(), 3);
		msg += _questions[_currQuestionIndex]->getAnswers()[1];
		msg += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getAnswers()[2].size(), 3);
		msg += _questions[_currQuestionIndex]->getAnswers()[2];
		msg += Helper::getPaddedNumber(_questions[_currQuestionIndex]->getAnswers()[3].size(), 3);
		msg += _questions[_currQuestionIndex]->getAnswers()[3];
	}
	catch (...)
	{
		msg = "1180";// need to send only for admin
	}
	for (auto player : _players)
	{
		player->send(msg);
	}
}
