#include "User.h"

User::User(string userName, SOCKET client_socket)
{
	_userName = userName;
	_sock = client_socket;
	_currRoom = nullptr;
}

User::~User()
{
	//remove shit
}

void User::send(string msg) { Helper::sendData(_sock, msg); }//check if i've used func correct

string User::getUserName()
{
	return _userName;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* currGame)
{
	_currGame = currGame;
}


void User::clearRoom()
{
	_currRoom = NULL;
}

bool User::createRoom(int roomID, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr)
	{
		//send message about failure by protocol
	}
	else //create a room
	{
		_currRoom = new Room(roomID, this, roomName, maxUsers, questionsNo, questionTime);//check im sending correct parameters
		//send succeed message
	}
	return true;//check this

}

bool User::joinRoom(Room* currRoom)
{
	_currRoom = currRoom;
	return true;//check this
}

void User::leaveRoom()
{
	//Room::leaveRoom(this);
	_currRoom->leaveRoom(this);
	_currRoom = NULL;
}

int User::closeRoom()//unfinished
{
	bool isUserInRoom = _currRoom;
	int suceed = -1;
	if (isUserInRoom)
	{
		/*suceed = Room::closeRoom(this);
		if(suceed != -1) //room is closed
		{
		delete _currRoom;
		_currRoom = nullptr;
		}*/
	}
	return suceed;

}
bool User::leaveGame()//unfinished
{
	if (_currGame)
	{
		//Game::leaveGame(this);
		_currGame = nullptr;
		return true;
	}
	return false;//no active game	
}