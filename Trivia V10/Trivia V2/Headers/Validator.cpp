#include "Validator.h"
bool Validator::isPasswordValid(std::string pass)
{
	bool hasDigit = false;
	bool hasBigLetter = false;
	bool hasSmallLetter = false;
	for (auto currChar : pass)
	{
		if (isdigit(currChar))	hasDigit = true;//make sure the casting from char to int is happning in "isdigit"
		else if (isalpha(currChar))
		{
			if (currChar >= 'a' && currChar <= 'z')	hasSmallLetter = true;//small letter
			else if (currChar >= 'A' && currChar <= 'Z')	hasBigLetter = true;
		}
	}
	return (pass.size() >= 4 && std::string::npos == pass.find(' ') && hasBigLetter && hasSmallLetter);
}
bool Validator::isUserNameValid(std::string userName) { return (!isdigit(userName[0]) && (std::string::npos == userName.find(' '))); }