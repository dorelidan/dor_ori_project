#pragma once
#include "ReceivedMessage.h"
ReceivedMessage::ReceivedMessage(SOCKET sock, int messageCode)
{
	_messeageCode = messageCode;
	_sock = sock;
	_user = NULL;
}
ReceivedMessage::ReceivedMessage(SOCKET sock, int messageCode, vector<string> values)
{
	_messeageCode = messageCode;
	_sock = sock;
	_user = NULL;
	_values = values;
}
SOCKET ReceivedMessage::getSocket()
{
	return _sock;
}
User* ReceivedMessage::getUser()
{
	return _user;
}
void ReceivedMessage::setUser(User* user)
{
	_user = user;
}
int ReceivedMessage::getMessageCode()
{
	return _messeageCode;
}
vector<string>& ReceivedMessage::getValues()
{
	return _values;
}