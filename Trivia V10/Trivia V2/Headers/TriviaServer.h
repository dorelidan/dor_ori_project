#pragma once
#include "User.h"
#include "Room.h"
#include "DataBase.h"
#include "ReceivedMessage.h"
#include <map>
#include <queue>
#include <thread>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include "Helper.h"
#include "Helper2.h"
#include "sqlite3.h"
#include <sstream>
#include <algorithm>
#include <math.h>




using namespace std;

class TriviaServer 
 {
	public:
	TriviaServer();
	~TriviaServer();
	void serve();
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(ReceivedMessage*);
	User* handleSignin(ReceivedMessage*);
	bool handleSignup(ReceivedMessage*);
	void handleSignout(ReceivedMessage*);

	void handleLeaveGame(ReceivedMessage*);
	void handleStartGame(ReceivedMessage*);
	void handlePlayerAnswer(ReceivedMessage*);
	bool handleCreateRoom(ReceivedMessage*);
	bool handleCloseRoom(ReceivedMessage*);
	bool handleJoinRoom(ReceivedMessage*);
	bool handleLeaveRoom(ReceivedMessage*);
	void handleGetUsersInRoom(ReceivedMessage*);
	void handleGetRooms(ReceivedMessage*);
	void handleBestScores(ReceivedMessage*);
	void handleGetPersonalStatus(ReceivedMessage*);
	void handleReceivedMessages();
	void addReceivedMessage(ReceivedMessage*);
	ReceivedMessage* buildReciveMessage(SOCKET, int);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET socket);
	Room* getRoomById(int);
	string newUser(string username, string pass);//using db class
	int getNewid();
	Room* getRoomByAdmin(User* admin);

	private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomList;
	mutex _mtxReceivedMessages;
	queue<ReceivedMessage*> _queRcvMessages;
	int _roomIdSequence;
};