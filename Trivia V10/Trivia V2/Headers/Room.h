#pragma once
#include <vector>
#include <iostream>
#include "User.h"
using namespace std;
class Game;
class User;
class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime);
	~Room();
	bool joinRoom(User* newUser);
	void leaveRoom(User* removeFromRoom);
	int closeRoom(User* u);
	vector<User*> getUsers();
	string getUsersListMessage();
	int gestQuestionsNo();
	int getId();
	string getName();
	User* getAdmin();
	void startGame();


private:
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string msg);
	void sendMessage(User* excludeUser, string msg);
	vector<User*> _users;//all users in curr room
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	int _id;
	string _name;
	DataBase _db;
};