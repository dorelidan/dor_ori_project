#pragma once
#include <iostream>
#include <string>
using namespace std;
class Validator 
{
public:
bool static isPasswordValid(string);
bool static isUserNameValid(string);
private:
protected:
//need to define class as static.;
};