#include "Helper2.h"

// recieves the type code of the message from socket (first byte)
// and returns the code. if no message found in the socket returns 0
unsigned char Helper2::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 1);
	string msg(s);
	delete s;

	if (msg == "")
		return 0;

	unsigned char res = msg[0];
	return res;
}


// Code: 200
// this function recieves the username from the socket and returns it (message code:200)
// if the message code not 200, throws exception
string Helper2::getUsername(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 1);

	string type(s);
	delete s;

	unsigned char code = type[0];
	if (code != MT_CLIENT_LOG_IN)
		throw exception("Invalid message type recieved");
	unsigned char userSize = (unsigned char)getPartFromSocket(sc, 1)[0];
	if (userSize == 0)
		return "";
	string username = getPartFromSocket(sc, userSize);
	return username;
	

}

// recieves the data of the file from the socket (when it is message 204)
// important!!! should use this function after getMessageTypeCode has been called
string Helper2::getFileData(SOCKET sc)
{
	char* res = getPartFromSocket(sc, 4);
	string s(res);

	if (res[0] == 0)
		return "";

	int size = 0;
	
	size = (size << 8) + (unsigned char)res[3];
	size = (size << 8) + (unsigned char)res[2];
	size = (size << 8) + (unsigned char)res[1];
	size = (size << 8) + (unsigned char)res[0];
	delete res;

	string data = getPartFromSocket(sc, size);
	return data;
}


// Code: 101
// this function send the socket update message (101) according the protocol
void Helper2::sendUpdateMessage(SOCKET sc, string fileContent, string currUser, string nextUser, int position)
{
	string res;

	char code = 101;
	int fileSize = fileContent.size();

	char bytes[4];
	copy(static_cast<const char*>(static_cast<const void*>(&fileSize)),
		static_cast<const char*>(static_cast<const void*>(&fileSize)) + 4,
		bytes);

	char currUserSize = currUser.size();
	char nextUserSize = nextUser.size();
	

	res += code;
	res += bytes[0];
	res += bytes[1];
	res += bytes[2];
	res += bytes[3];


	res += fileContent;
	res += currUserSize;
	res += currUser;
	res += nextUserSize;
	res += nextUser;

	copy(static_cast<const char*>(static_cast<const void*>(&position)),
		static_cast<const char*>(static_cast<const void*>(&position)) + 4,
		bytes);

	res += bytes[0];
	res += bytes[1];
	res += bytes[2];
	res += bytes[3];


	sendData(sc, res);

}




// send data to socket
// this is private function
void Helper2::sendData(SOCKET sc, string message) 
{
	const char* data = message.c_str();
	
	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
		throw exception("Error while sending message to client");
}

// recieve data from socket according byteSize
// this is private function
char* Helper2::getPartFromSocket(SOCKET sc, int bytesNum)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		string s = "Error while recieving from socket: ";
		s += to_string(sc);
		throw exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;


}
