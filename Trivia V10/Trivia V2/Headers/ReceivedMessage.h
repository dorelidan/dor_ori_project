#pragma once
#include <iostream>
#include <string>
#include <map>
#include <queue>
#include "User.h"
#include "Room.h"

using namespace std;
class User;
class ReceivedMessage
{
public:
	ReceivedMessage(SOCKET sock, int messageCode);
	ReceivedMessage(SOCKET sock, int messageCode, vector<string> values);
	SOCKET getSocket();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();
private:
	SOCKET _sock;
	User* _user;
	int _messeageCode;
	vector<string> _values;
};