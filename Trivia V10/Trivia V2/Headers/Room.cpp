#include "Room.h"
using namespace std;
Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime) :_id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionNo(questionNo), _questionTime(questionTime)
{
	_users.push_back(admin);
}
Room::~Room(){}
bool Room::joinRoom(User* newUser)
{
	//1.is room full?
	if (_users.size() < _maxUsers)	//1.1add to _users vector, send him success message send message message to all connected users
	{
		string messageToNewUser = "1100" + Helper::getPaddedNumber(_questionNo, 2) + Helper::getPaddedNumber(_questionTime, 2);
		_users.push_back(newUser);
		newUser->send(messageToNewUser);
		sendMessage(getUsersListMessage());//update all users
		return true;
	}
	else//0.9[FALSE] send a falire message to this user
	{
		newUser->send("1101");
		return false;
	}
	//when I should send "1102"? :O :o :O
}
void Room::leaveRoom(User* removeFromRoom)
{

	///go back to this


	cout << _users[0]->getUserName();
	/*cout << "test1";
	for (auto user : _users) {
		cout << user->getUserName();
	}

		cout << "test2";
		//_users.erase(_users.begin() + i);
		cout << "test3";
		//
		//this is maybe not specific enough, there might be more messages we need to send.
		removeFromRoom->send("1120");//success message
		//
		//
		sendMessage(getUsersListMessage());*/
}
int Room::closeRoom(User* u)
{
	if (u != _admin)	return -1;
	//closing the room:.....
	sendMessage("116");
	for (auto currUser : _users)	if (currUser != u)	currUser->clearRoom();
	return _id;
}
vector<User*> Room::getUsers()
{
	return _users;
}
string Room::getUsersListMessage()//done
{
	string connectedUsers = "108";
	connectedUsers.append(to_string(_users.size()));
	for (auto user : _users)
	{
		connectedUsers += Helper::getPaddedNumber(user->getUserName().size(), 2);
		connectedUsers += user->getUserName();
	}

	return connectedUsers;
}
int Room::gestQuestionsNo()
{
	return _questionNo;
}
int Room::getId()
{
	return _id;
}
string Room::getName()//make sure we're returning the right thing
{
	return _name;
}
string Room::getUsersAsString(vector<User*> a, User* b) { return "s"; }//not a must
void Room::sendMessage(string msg)
{
	for (auto user : _users)
	{
		user->send(msg);
	}
}
void  Room::sendMessage(User* excludeUser, string msg)
{
	for (auto currUser : _users)
	{
		if (currUser != excludeUser) currUser->send(msg);
	}

}

User* Room::getAdmin()
{
	return _admin;
}

void Room::startGame()
{
	Game* game = new Game(_users, _questionNo, _db);//db here
	for (auto currUser : _users)
	{
		currUser->setGame(game);
	}
}