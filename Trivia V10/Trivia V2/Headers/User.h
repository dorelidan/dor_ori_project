#pragma once
#include <string>
#include "Helper.h"
#include "Game.h"
#include "Room.h"
using namespace std;

class User 
{
public:
	User(string userName, SOCKET client_socket);
	~User();
	void send(string);
	string getUserName();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* currGame);
	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
private:
	string _userName;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};