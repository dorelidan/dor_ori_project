#include "Question.h"
#define NOT_EXISTS 0 
#define EXISTS -1
//need to fix con to randomly shuffle answers
Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4):_id(id), _question(question)
{
	int freeIndexes[4] = { NOT_EXISTS };//0 = this place is free, -1 = the place is occupied already.
	//should place the values randomnly:
	_answers[0] = correctAnswer;
	_answers[1] = answer2;
	_answers[2] = answer3;
	_answers[3] = answer4;
	for (int i = 0; i < 4; i++)
	{
		if (_answers[i] == correctAnswer)
		{
			_correctAnswerIndex = i + 1;
			break;
		}
	}
}
string Question::getQuestion() { return _question; }
string* Question::getAnswers(){ return _answers; }
int Question::getCorrectAnswerIndex(){	return _correctAnswerIndex; }
int Question::getId() { return _id; }
