#include "DataBase.h"

unordered_map<string, vector<string>> DataBase::_results;
static int callback(void* NotUsed, int argc, char** argv, char **azColName);
int callbackString(void* notUsed, int argc, char** argv, char** azCol); //The value that was called back will be stored outside
string outsideString;
string lastResult;
DataBase::DataBase()
{
	rc = sqlite3_open(DB_FILE_NAME, &_db);
	try {
		string sql = "SELECT email FROM t_users WHERE username = \"user\";";
		rc = sqlite3_exec(_db, sql.c_str(), callback, 0, &zErrMsg);
		cout << lastResult;
	}
	catch (...)
	{
		cout << "cons crashed";
	}
	currGameID = 1;
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

int DataBase::callbackWriteDB(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = _results.find(azCol[i]);
		if (it != _results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			_results.insert(p);
		}
	}

	return 0;

}

void DataBase::clearTable()
{
	for (auto it = _results.begin(); it != _results.end(); ++it)
	{
		it->second.clear();
	}
	_results.clear();
}

bool DataBase::isUserExists(string username)
{
	char *errMsg;
	clearTable();
	int rc = sqlite3_exec(_db, ("select username from t_users where username = \"" + username + "\";").c_str(), callbackWriteDB, nullptr, &errMsg);;
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		throw exception(str.c_str());
	}

	if (_results.size())
		return true;
	return false;
}


bool DataBase::isUserAndPassMatch(string username, string password)
{
	if (!isUserExists(username))
		return false;
	char *errMsg;
	clearTable();
	int rc = sqlite3_exec(_db, ("select username,password from t_users where username = \"" + username + "\" and password = \"" + password + "\";").c_str(), callbackWriteDB, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		throw exception(str.c_str());
	}

	if (_results.size())
		return true;
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	char* errMsg;
	cout << "Add new user:\ninsert into t_users(username,password,email) values (\"" + username + "\", \"" + password + "\",\"" + email + "\");";
	int rc = sqlite3_exec(_db, ("insert into t_users(username,password,email) values (\"" + username + "\", \"" + password + "\",\"" + email + "\");").c_str(), nullptr, nullptr, &errMsg);;
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		return false;
	}
	return true;
}

vector<Question*> DataBase::initQuestions(int questionsNum)
{
	srand(time(NULL));
	vector<Question*> questions;
	char *errMsg;
	clearTable();
	string command = "select * from t_questions;";
	int rc = sqlite3_exec(_db, command.c_str(), callbackWriteDB, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		throw exception(str.c_str());
	}
	for (int i = 0, j = 0; i < questionsNum; i++)
	{
		//Question q = new Question
	}
}
int callbackString(void* notUsed, int argc, char** argv, char** azCol) //The value that was called back will be stored outside
{
	outsideString = "";
	if (argv[0] == NULL)
	{
		return -1;
	}

	for (int i = 0; i < argc; i++)
	{
		outsideString += argv[i];
		if (i != argc - 1)
		{
			outsideString += " ";
		}
	}


	return 1;
}

vector<string> DataBase::getPersonalStatus(string username) //bloop
{
	char *errMsg;
	std::vector<string> empty;
	if (isUserExists(username))
	{
		

		string output = "";
		string query = "select count (distinct game_id) from t_players_answers where username = '" + username + "';";
		string query2 = "select count ( is_correct) from t_players_answers where username = '" + username + "' and is_correct = 1;";
		string query3 = "select count ( is_correct) from t_players_answers where username = '" + username + "' and is_correct = 0;";
		string query4 = "select avg( answer_time) from t_players_answers where username = '" + username + "';";
		const char* c_line1 = query.c_str();
		const char* c_line2 = query2.c_str();
		const char* c_line3 = query3.c_str();
		const char* c_line4 = query4.c_str();
		int rc2 = sqlite3_exec(_db, c_line1, callbackString, nullptr, &errMsg);
		if (rc2 != -1)
		{
			output = outsideString;
			empty.push_back(output);
			output = "";
		}
		else
		{
			empty.push_back("");
		}
		rc2 = sqlite3_exec(_db, c_line2, callbackString, nullptr, &errMsg);
		if (rc2 != -1)
		{
			output = outsideString;
			empty.push_back(output);
			output = "";
		}
		else
		{
			empty.push_back("");
		}
		rc2 = sqlite3_exec(_db, c_line3, callbackString, nullptr, &errMsg);
		if (rc2 != -1)
		{
			output = outsideString;
			empty.push_back(output);
			output = "";
		}
		else
		{
			empty.push_back("");
		}
		rc2 = sqlite3_exec(_db, c_line4, callbackString, nullptr, &errMsg);
		if (rc2 != -1)
		{
			output = outsideString;
			empty.push_back(output);
			output = "";
		}
		else
		{
			empty.push_back("");
		}


		sqlite3_free(_db);
	}

	return empty;
}

vector<string> DataBase::getBestScores()
{
	vector<string> bestUsers;
	char *errMsg;
	clearTable();
	string query = "SELECT count(is_correct) FROM t_players_answers where username and is_correct = 1;";
	int rc = sqlite3_exec(_db, query.c_str(), callbackWriteDB, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		throw exception(str.c_str());
	}
}

int DataBase::insertNewGame()
{
	char* errMsg;
	int rc = sqlite3_exec(_db, ("insert into t_games(game_id,status,start_time,end_time) values (" + to_string(currGameID) + ", 0 ," + to_string(time(NULL)) + ");").c_str(), nullptr, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		throw exception(str.c_str());
	}
	return currGameID++;
}

bool DataBase::updateGameStatus(int gameID)
{
	char* errMsg;
	string query = "update t_games set status = 1, and time =" + to_string(time(NULL)) + " where game_id =" + to_string(gameID) + ";";
	int rc = sqlite3_exec(_db, query.c_str(), nullptr, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameID, string username, int questionID, string answer, bool isCorrect, int answerTime)
{
	char* errMsg;
	string command = "insert into t_players_answers (game_id,username,question_id,player_answer,is_correct,answer_time) values(" + to_string(gameID) + ",\"" + username + "\"," + to_string(questionID) + ",\"" + answer + "\"," + to_string(isCorrect) + "," + to_string(answerTime) + ");";
	int rc = sqlite3_exec(_db, command.c_str(), nullptr, nullptr, &errMsg);
	if (rc != SQLITE_OK)
	{
		string str = "SQL Error: " + string(sqlite3_errmsg(_db));
		sqlite3_free(_db);
		return false;
	}
	return true;
}

int DataBase::addNewUserRetCode(string username, string password, string email)
{
	if (!Validator::isPasswordValid(password)) {
		return 1041;//pass ILIGEL
	}
	else if (!Validator::isUserNameValid(username))   return 1043;// user name ILIGEL
	else if (isUserExists(username))   return 1042;
	else
	{
		if (addNewUser(username, password, email)) return 1040;
		else
		{
			return 1044;//if we got false its mean the addNewUser throw exception AND we know its not because of password valid or userName valid or nameExist because we already checked it in the if block.
		}
	}
}

static int callback(void* NotUsed, int argc, char** argv, char **azColName) {
	int i = 0;
	for (i = 0; i<argc; i++) {
		//printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		lastResult = argv[i] ? argv[i] : "NULL";
	}
	printf("\n");
	return 0;
}
