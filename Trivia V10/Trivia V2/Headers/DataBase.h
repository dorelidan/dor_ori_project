#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <exception>
#include <unordered_map>
#include <map>
#include <time.h>
#include <random>
#include "Question.h"
#include "Validator.h"
#include "sqlite3.h"

#define DB_FILE_NAME "trivia.db"
using namespace std;
class DataBase
{
public:
	DataBase();
	~DataBase();

	bool addNewUser(string username, string password, string email);
	bool isUserExists(string username);
	bool isUserAndPassMatch(string username, string password);

	vector<Question*> initQuestions(int numOfQuestions);

	int insertNewGame();

	vector<string> getBestScores();
	vector<string> getPersonalStatus(string username);
	int addNewUserRetCode(string username, string password, string email);
	bool updateGameStatus(int gameID);
	bool addAnswerToPlayer(int gameID, string username, int questionID, string answer, bool isCorrect, int answerTime);
	sqlite3* _db; // need to be private when debugging is done.  
	int rc;
	char *zErrMsg = 0;
private:

	// THE CALLBACK TO END ALL CALLBACKS
	static int callbackWriteDB(void* notUsed, int argc, char** argv, char** azCol);
	static void clearTable();

	//static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	//static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	//static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	//static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);

	static unordered_map<string, vector<string>> _results;
	int currGameID;
};