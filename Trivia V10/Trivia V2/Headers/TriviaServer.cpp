#include "TriviaServer.h"

static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;

TriviaServer::TriviaServer()
{
	/// Database constructor here!
	DataBase();
	TRACE("Starting...");

	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - socket");

	serve();
}

TriviaServer::~TriviaServer()
{
	for(auto currConnectedUser : _connectedUsers)  _connectedUsers.erase(currConnectedUser.first);
	
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	
	while (true)
	{
		try
		{
			_db = *(new DataBase());
			int code = Helper::getMessageTypeCode(client_socket);
			ReceivedMessage m(client_socket, code);
			code = m.getMessageCode();
			TRACE("(%d)", code);
			if (code == 200) // Ask to sign in
			{
				int username_length = Helper::getIntPartFromSocket(client_socket, 2);
				string username = Helper::getStringPartFromSocket(client_socket, username_length);
				int password_length = Helper::getIntPartFromSocket(client_socket, 2);
				string password = Helper::getStringPartFromSocket(client_socket, password_length);
				cout << username + "   " + password;
				if (getUserByName(username) != NULL)
				{
					Helper::sendData(client_socket, "1022"); // user already connected
				}
				else
				{
					if (_db.isUserAndPassMatch(username, password))
					{
						User* user = new User(username, client_socket);
						_connectedUsers.insert(pair<SOCKET, User*>(client_socket, user));
						Helper::sendData(client_socket, "1020"); // success
					}
					else
					{
						Helper::sendData(client_socket, "1021"); // wrong details
					}
				}

			}
			else if (code == 201) // Ask to sign out
			{
				handleSignout(&m);
			}
			else if (code == 203) // Ask to sign up
			{
				int username_length = Helper::getIntPartFromSocket(client_socket, 2);
				string username = Helper::getStringPartFromSocket(client_socket, username_length);
				int password_length = Helper::getIntPartFromSocket(client_socket, 2);
				string password = Helper::getStringPartFromSocket(client_socket, password_length);
				int email_length = Helper::getIntPartFromSocket(client_socket, 2);
				string email = Helper::getStringPartFromSocket(client_socket, email_length);
				string ret_code = to_string(_db.addNewUserRetCode(username, password, "email"));
				Helper::sendData(client_socket, ret_code); // success
			}
			else if (code == 205) // ask for room list
			{
				map<int, Room*>::iterator it;
				int room_count = 0;
				string message = "";
				for (it = _roomList.begin(); it != _roomList.end(); it++)
				{
					room_count += 1;
				}
				
				message += "106" + Helper::getPaddedNumber(room_count, 4);
				for (it = _roomList.begin(); it != _roomList.end(); it++)
				{
					message += Helper::getPaddedNumber(it->first, 4);
					message += Helper::getPaddedNumber(it->second->getName().size(), 2);
					message += it->second->getName();
				}
				Helper::sendData(client_socket, message);

			}
			else if (code == 207) // ask for users in room
			{
				try
				{
				int room_id = Helper::getIntPartFromSocket(client_socket, 4);
					Helper::sendData(client_socket, _roomList.find(room_id)->second->getUsersListMessage());
				}
				catch(...)
				{
					Helper::sendData(client_socket, "1080");
				}
			}
			else if (code == 209)//join
			{
				int room_id = Helper::getIntPartFromSocket(client_socket, 4);
				if (getRoomById(room_id) != NULL)
				{
					getRoomById(room_id)->joinRoom(getUserBySocket(client_socket));
				}
				else
				{
					Helper::sendData(client_socket, "1102");
				}
			}
			else if (code == 211)//leave
			{
				getUserBySocket(client_socket)->leaveRoom();//116? 119?
			}
			else if (code == 213)
			{
				try
				{
					int room_sizename = Helper::getIntPartFromSocket(client_socket, 2);
					string room_name = Helper::getStringPartFromSocket(client_socket, room_sizename);
					int room_nplayer = Helper::getIntPartFromSocket(client_socket, 1);
					int room_nquestion = Helper::getIntPartFromSocket(client_socket, 2);
					int room_time = Helper::getIntPartFromSocket(client_socket, 2);
					int room_id = getNewid();
					Room* room = new Room(room_id, getUserBySocket(client_socket), room_name, room_nplayer, room_nquestion, room_time);
					_roomList.insert((pair<int, Room*>(room_id, room)));
					Helper::sendData(client_socket, "1140");
				}
				catch (...)
				{
					Helper::sendData(client_socket, "1141");
				}
			}
			else if (code == 215)
			{
				handleCloseRoom(&m);
				//getRoomByAdmin(getUserBySocket(client_socket))->closeRoom(getUserBySocket(client_socket));
			}
			else if (code == 217)
			{
				getRoomByAdmin(getUserBySocket(client_socket))->startGame();
			}
			else if (code == 219)
			{
				int user_q = Helper::getIntPartFromSocket(client_socket, 1);
				int user_time = Helper::getIntPartFromSocket(client_socket, 2);
				getUserBySocket(client_socket)->getGame()->handleAnswerFromUser(getUserBySocket(client_socket),user_q,user_time);
			}
			else if (code == 223)//Best scores [124 ## userName highestScore ## userName highestScore ## userName highestScore]
			{
				handleBestScores(&m);
			}
			else if (code == 222)//leave game
			{
				handleLeaveGame(&m);
			}
			// START MY STATUS
			else if (code == 225)
			{
				vector<string> info = _db.getPersonalStatus(getUserBySocket(client_socket)->getUserName());
				string average = info[3];
				float num = stof(info[3]);
				while (fmod(num, 10) != 0)
				{
					num = num * 10;
				}
				string str = to_string(num);
				if (str.size() == 1)
				{
					average = "000" + str;
				}
				else if (str.size() == 2)
				{
					average = "00" + str;
				}
				else if (str.size() == 3) average = "0" + str;
				else
				{
					average = "0000";
				}
				cout << average;
				Helper::sendData(client_socket, "126" + Helper::getPaddedNumber(stoi(info[0]),4) + Helper::getPaddedNumber(stoi(info[1]), 6) + Helper::getPaddedNumber(stoi(info[2]), 6) + Helper::getPaddedNumber(stoi(average), 4));
			}
			// END MY STATUS
			else//safe delete user
			{
				safeDeleteUser(&m);
			}


		}
		catch (const std::exception& e)
		{
			//safeDeleteUser(&m);
			std::cout << "Exception was catch in function: " << e.what() << std::endl;
			TRACE("notify all");
			break;
			//_doc_was_edit.notify_all();
		}
	}
}


void TriviaServer::safeDeleteUser(ReceivedMessage* msg)//bloop
{
	try
	{
		SOCKET soc = msg->getSocket();
		handleSignout(msg);
		_connectedUsers.erase(_connectedUsers.find(soc));
		closesocket(soc);
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}
User* TriviaServer::handleSignin(ReceivedMessage*) { return NULL;  }
bool TriviaServer::handleSignup(ReceivedMessage*){ return NULL; }
void TriviaServer::handleSignout(ReceivedMessage* msg)
{
	if (_connectedUsers.find(msg->getSocket()) != _connectedUsers.end())
	{
		if (handleCloseRoom(msg))
			if (handleLeaveRoom(msg))
				handleLeaveGame(msg);
	}
}
void TriviaServer::handleLeaveGame(ReceivedMessage* msg){getUserBySocket(msg->getSocket())->leaveGame();}//bloop
void TriviaServer::handleStartGame(ReceivedMessage* ){}
void TriviaServer::handlePlayerAnswer(ReceivedMessage* ){}
bool TriviaServer::handleCreateRoom(ReceivedMessage* ){ return NULL; }
bool TriviaServer::handleCloseRoom(ReceivedMessage* msg)
{
	if (msg->getUser())
	{
		if (msg->getUser()->getRoom())
		{
			if (msg->getUser()->closeRoom() != (-1))
			{
				std::map<int, Room*>::iterator it;
				int help = (msg->getUser()->getRoom()->getId());
				it = _roomList.find(help);
				_roomList.erase(it);
				return true;
			}
		}
	}
	return false;
}
bool TriviaServer::handleJoinRoom(ReceivedMessage* ){ return NULL; }
bool TriviaServer::handleLeaveRoom(ReceivedMessage* ){ return NULL; }
void TriviaServer::handleGetUsersInRoom(ReceivedMessage* ){}
void TriviaServer::handleGetRooms(ReceivedMessage* ){}
void TriviaServer::handleBestScores(ReceivedMessage* msg)
{
	vector<std::string> bestScores = _db.getBestScores();
	std::string s = "124";
	std::string username = "", score = "";
	int size = bestScores.size();;
	for (int i = 0; i < size; i++)
	{
		std::string temp = bestScores[i];
		char c = '4';
		int spot = 0;
		while (temp[spot] != ' ')
		{
			c = temp[spot];
			username += c;
			spot++;

		}
		spot++;
		while (spot != temp.size())
		{
			c = temp[spot];
			score += c;
			spot++;

		}


		s = s + Helper::getPaddedNumber(username.size(), 2) + username + Helper::getPaddedNumber(std::stoi(score), 6);
		username = "";
		score = "";
	}
	size = bestScores.size();
	for (int i = 0; i < 3 - size; i++)
		s += "00";
	Helper::sendData(msg->getSocket(), s);

}
void TriviaServer::handleGetPersonalStatus(ReceivedMessage* msg)
{
	std::vector<std::string> v = _db.getPersonalStatus(msg->getUser()->getUserName());
	std::string msgToSend = "126" + v[0] + v[1] + v[2] + v[3];
	Helper::sendData(msg->getSocket(), msgToSend);
}
void TriviaServer::handleReceivedMessages(){}
void TriviaServer::addReceivedMessage(ReceivedMessage* ){}
ReceivedMessage* TriviaServer::buildReciveMessage(SOCKET, int){ return NULL; }
User* TriviaServer::getUserByName(string username)
{
	for (auto it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
		if (it->second->getUserName() ==  username)
			return it->second;

	return NULL;
}
User* TriviaServer::getUserBySocket(SOCKET socket) 
{
	auto iterator = _connectedUsers.find(socket);
	return iterator->second;
 }
Room* TriviaServer::getRoomById(int id)
{
	auto iterator = _roomList.find(id);
	return iterator->second;
}
string TriviaServer::newUser(string username, string pass)
{
	DataBase a;//creating temp object inOrder to add user and get valid return code AND using dataBase class [its ok to define it on stack because the data is saved in the db.txt anyway and not on ram]
	return to_string(a.addNewUserRetCode(username, pass, "email"));
}

int TriviaServer::getNewid()
{
	_roomIdSequence += 1;
	return _roomIdSequence;
}

Room* TriviaServer::getRoomByAdmin(User* admin)
{
	for (auto currRoom : _roomList)
	{
		if (currRoom.second->getAdmin() == admin) return currRoom.second;
	}
	return NULL;
}