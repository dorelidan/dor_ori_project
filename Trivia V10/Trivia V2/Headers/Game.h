#pragma once
#include "DataBase.h"
#include <vector>
#include <map>
#include <iostream>
#include "User.h"
using namespace std;
class Room;
class User;
class Game 
{
public:
	Game(const vector<User*>& players, int questionNo, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int que_n, int que_time);
	bool leaveGame(User*);
	int getID();
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currTurnAnswers;
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionsToAllUsers();
};