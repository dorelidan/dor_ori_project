#pragma comment(lib, "Ws2_32.lib")

#include "Headers/WSAInitializer.h"
#include "Headers\TriviaServer.h"

int main()
{
	try
	{

		TRACE("Starting...");
		// NOTICE at the end of this block the WSA will be closed 
		WSAInitializer wsa_init;
		TriviaServer* server = new TriviaServer();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}

}